# Git Rules

git clone https://gitlab.com/CharlotteLinaGauthier/freego-back.git

    git checkout brancheName
    .....
    git status
    git add  
    git commit -m 'UC_XXXXXXXXX:XXXXXXXX'   
    git push  

## Branches
    Main                        Main
        Dev                      Dev
            Features              UC_1_Connexion
                Task : XXX           T1_Create_Page 
                Task : XXX           T2_Connect_to_Back 

**NE JAMAIS PUSH/COMMIT SUR LES BRANCHES DEV OU MAIN.**  
Si un besoin de correction urgente créer une branche hotFixe à partir de Dev ou Main 
  
**Dev** : Branche principale du développement  
**Features** : User Case or User Technical (Exemple : UC_Authentification), création des features en debut de sprint ensemble par rapport au UC choisis   
**Tasks** : Les différentes tâches pour un même UC 


## Merge 
Les merges des Tasks vers une Features sont gérés par l'équipe ou la personne qui est en charge. 

Une task merger est une task fini ! Donc prioriser la création de petite task !!! On peut retrouver autant de task que l'on veut sur une feature. 

Les merges des features vers Dev doivent être assigné à au moins deux personnes différentes de celle qui demande afin de valider ce qui a était fait et partager.

Les merges de Dev vers Main ce fait en fin de sprint quand on délivre des versions, la validation sera assigné à l'équipe entière. 

## Avant de merge 

Observer les branches sur git, si votre branche A à un retard par rapport à la branche B d'ou elle est tirer initialement c'est juste qu'un merge à était fait entre temps , pas de panique ! 

    git checkout brancheA
    git commit -m " Vos modifications"
    git push 

    git checkout brancheB
    git pull 

    git checkout brancheA
    git rebase origin/brancheB

    //Vérifier qu'il n'y ai aucun conflit ! 


Eviter de travailler sur des fichiers similaires en même temps pour éviter les conflits au niveau du merge, COMMUNIQUER dans une équipe pour travailler sur des petites tâches et donc limité les conflits ! 
