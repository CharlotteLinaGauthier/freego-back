package com.example.freego.common.DTO;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;

public class UserDtoWithPassword extends UserDto implements UserDetails, Serializable {


    private String pwd;

    public UserDtoWithPassword() {
    }

    public UserDtoWithPassword( String pwd) {
        super();
        this.pwd = pwd;
    }

    public UserDtoWithPassword(UserDto user, String pwd){
        super(user.getId(),user.getLogin(), user.getLastName(), user.getSurName(), user.getEmail(),new PreferenceDto(user.getPreferenceId(), user.getPreferenceName()),user.getAllergies(),user.getIdfriends());
        this.pwd = pwd;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.pwd;
    }

    @Override
    public String getUsername() {
        return this.getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return super.toString() +
                ",pwd='" + pwd + '\'' +
                '}';
    }

    public UserDto toUserDto(){
        return new UserDto(this.getId(),this.getLogin(),  this.getLastName(), this.getSurName(), this.getEmail(),new PreferenceDto(this.getPreferenceId(),this.getPreferenceName()),this.getAllergies(),this.getAllergies());
    }
}
