package com.example.freego.common.Exception;

public class FunctionalException extends Exception{

    public FunctionalException(String message) {
        super(message);
    }

    public FunctionalException() {
    }

    public FunctionalException(String message, Throwable cause) {
        super(message, cause);
    }
}
