package com.example.freego.common.DTO;

import java.util.Date;

public class FridgeFoodDto extends FoodDto {
    private Long id;
    private Date dlc;
    private Integer quantity;
    private Long foodId;

    public FridgeFoodDto() {
    }

    public FridgeFoodDto(Long id, Date dlc, Integer quantity, Long foodId) {
        this.id = id;
        this.dlc = dlc;
        this.quantity = quantity;
        this.foodId = foodId;
    }

    public FridgeFoodDto(FoodDto food, Long id, Date dlc, Integer quantity, Long foodId) {
        super(food.getId(),food.getName(),food.getImgurl(),food.getTagsId(),food.getTagsDescription());
        this.id = id;
        this.dlc = dlc;
        this.quantity = quantity;
        this.foodId = foodId;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDlc() {
        return dlc;
    }

    public void setDlc(Date dlc) {
        this.dlc = dlc;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getFoodId() {
        return foodId;
    }

    public void setFoodId(Long foodId) {
        this.foodId = foodId;
    }
}
