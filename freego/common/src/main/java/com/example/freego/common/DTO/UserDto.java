package com.example.freego.common.DTO;

import java.util.HashSet;
import java.util.Set;

public class UserDto {

    private Long id;
    private String login;
    private String lastName;
    private String surName;
    private String email;
    private Long preferenceId;
    private String preferenceName;
    private Set<Long> allergies = new HashSet<>();
    private Set<Long> idfriends = new HashSet<>();

    public UserDto() {
    }

    public UserDto(Long id, String login, String lastName, String surName, String email,PreferenceDto preference,Set<Long>allergies, Set<Long> idfriends) {
        this.id = id;
        this.login = login;
        this.lastName = lastName;
        this.surName = surName;
        this.email = email;
        this.preferenceName = preference.getName();
        this.preferenceId = preference.getId();
        this.allergies = allergies;
        this.idfriends = idfriends;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    public Set<Long> getAllergies() {
        return allergies;
    }

    public void setAllergies(Set<Long> allergies) {
        this.allergies = allergies;
    }

    public Set<Long> getIdfriends() {
        return idfriends;
    }

    public void setIdfriends(Set<Long> idfriends) {
        this.idfriends = idfriends;
    }

    public Long getPreferenceId() {
        return preferenceId;
    }

    public void setPreferenceId(Long preferenceId) {
        this.preferenceId = preferenceId;
    }

    public String getPreferenceName() {
        return preferenceName;
    }

    public void setPreferenceName(String preferenceName) {
        this.preferenceName = preferenceName;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"login\":\"" + login + '"' +
                ", \"lastName\":\"" + lastName + '"' +
                ", \"surName\":\"" + surName + '"' +
                ", \"email\":\"" + email + '"' +
                ", \"preferenceId\":" + preferenceId +
                ", \"preferenceName\":\"" + preferenceName +'"'+
                ", \"allergies\":" + allergies +
                ", \"idfriends\":" + idfriends +
                '}';
    }
}
