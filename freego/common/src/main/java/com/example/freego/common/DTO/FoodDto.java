package com.example.freego.common.DTO;

public class FoodDto {



    private Long id;
    private String name;
    private String imgurl;
    private Long tagsId;
    private String tagsDescription;

    public FoodDto(){

    }

    public FoodDto(Long id, String name, String imgurl, Long tagsId, String tagsDescription) {
        this.id = id;
        this.name = name;
        this.imgurl = imgurl;
        this.tagsId = tagsId;
        this.tagsDescription = tagsDescription;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public Long getTagsId() {
        return tagsId;
    }

    public void setTagsId(Long tagsId) {
        this.tagsId = tagsId;
    }

    public String getTagsDescription() {
        return tagsDescription;
    }

    public void setTagsDescription(String tagsDescription) {
        this.tagsDescription = tagsDescription;
    }
}
