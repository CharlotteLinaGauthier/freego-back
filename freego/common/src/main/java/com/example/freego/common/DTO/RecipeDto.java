package com.example.freego.common.DTO;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RecipeDto {

    private Long id;
    private String name;
    private String imgurl;
    private String preparetime;
    private String cooktime;
    private String totaltime;
    private String directions;
    private Map<Long,String> idingredients = new HashMap<>();

    public RecipeDto() {
    }

    public RecipeDto(Long id, String name, String imgurl, String preparetime, String cooktime, String totaltime, String directions, Map<Long,String> idingredients) {
        this.id = id;
        this.name = name;
        this.imgurl = imgurl;
        this.preparetime = preparetime;
        this.cooktime = cooktime;
        this.totaltime = totaltime;
        this.directions = directions;
        this.idingredients = idingredients;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getPreparetime() {
        return preparetime;
    }

    public void setPreparetime(String preparetime) {
        this.preparetime = preparetime;
    }

    public String getCooktime() {
        return cooktime;
    }

    public void setCooktime(String cooktime) {
        this.cooktime = cooktime;
    }

    public String getTotaltime() {
        return totaltime;
    }

    public void setTotaltime(String totaltime) {
        this.totaltime = totaltime;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    public Map<Long, String> getIdingredients() {
        return idingredients;
    }

    public void setIdingredients(Map<Long, String> idingredients) {
        this.idingredients = idingredients;
    }
}
