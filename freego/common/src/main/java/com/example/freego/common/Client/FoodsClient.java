package com.example.freego.common.Client;



import com.example.freego.common.DTO.FoodDto;

import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashSet;
import java.util.List;


@FeignClient(name="foods",url="reverse-proxy/foods"/*,configuration = {FeignInterceptor.class}*/)

public interface FoodsClient {
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    FoodDto getFood(@PathVariable("id") Long id);

    @RequestMapping(method=RequestMethod.PUT,value="/{id}")
    void updateFood(@RequestBody FoodDto food);

    @RequestMapping(method = RequestMethod.GET, value = "/")
    List<FoodDto> getAllFoods();

    @RequestMapping(method = RequestMethod.POST, value = "/fridge")
    List<FoodDto> getFoodsByFridgeId(@RequestBody List<Long> ids);

}
