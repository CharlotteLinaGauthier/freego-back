package com.example.freego.common.Client;

import com.example.freego.common.DTO.RecipeDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@FeignClient(name="recipes",url="reverse-proxy/recipes"/*,configuration = {FeignInterceptor.class}*/)
public interface RecipesClient {

    @RequestMapping(method= RequestMethod.GET,value="/")
    List<RecipeDto> getAllRecipe();

    @RequestMapping(method= RequestMethod.GET,value="/{id}")
    RecipeDto getRecipe(@PathVariable String id);

    @RequestMapping(method= RequestMethod.POST,value="/")
    void addRecipe(@RequestBody RecipeDto recipe);

    @RequestMapping(method=RequestMethod.PUT,value="/{id}")
    void updateRecipe(@RequestBody RecipeDto recipe);

    @RequestMapping(method=RequestMethod.DELETE,value="/{id}")
    void deleteRecipe(@PathVariable String id);




}
