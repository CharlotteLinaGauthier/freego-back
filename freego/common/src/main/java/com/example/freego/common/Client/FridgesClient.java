package com.example.freego.common.Client;

import com.example.freego.common.DTO.FridgeDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name="fridges",url="reverse-proxy/fridges"/*,configuration = {FeignInterceptor.class}*/)
public interface FridgesClient {

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    FridgeDto getFridge(@PathVariable("id") Long id);

    @RequestMapping(method=RequestMethod.PUT,value="/")
        void updateFood(@RequestBody FridgeDto fridge);

    @RequestMapping(method = RequestMethod.GET, value = "/")
        List<FridgeDto> getAllFridges();

    @RequestMapping(method = RequestMethod.POST, value = "/")
        FridgeDto createFridge(@RequestBody FridgeDto fridge);


}
