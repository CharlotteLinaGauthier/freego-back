package com.example.freego.common.DTO;

import java.util.*;

public class FridgeDto {

    private Long id;
    private String name;
    private Long userId;
    private List<FridgeFoodDto> foodList = new ArrayList<FridgeFoodDto>();


    public FridgeDto(){

    }

    public FridgeDto(Long id, String name, Long userId) {
        this.id = id;
        this.name = name;
        this.userId = userId;
    }

    public FridgeDto(Long id, String name, Long userId, List<FridgeFoodDto> foodList) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.foodList = foodList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<FridgeFoodDto> getFoodList() {
        return foodList;
    }

    public void setFoodList(List<FridgeFoodDto> foodList) {
        this.foodList = foodList;
    }
}
