package com.example.freego.common.Client;

import com.example.freego.common.Config.FeignInterceptor;
import com.example.freego.common.DTO.UserDto;
import com.example.freego.common.DTO.UserDtoWithPassword;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "users", url = "reverse-proxy/users", configuration = { FeignInterceptor.class })
public interface UsersClient {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    List<UserDto> getUsers();

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    UserDto getUserById(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.POST, value = "/connect}")
    UserDto connectUser(@RequestBody UserDtoWithPassword userDTO);

    @RequestMapping(method = RequestMethod.POST, value = "/")
    UserDto createUser(@RequestBody UserDtoWithPassword user);

    @RequestMapping(method = RequestMethod.GET, value = "/{username}")
    UserDtoWithPassword getByUsername(@RequestHeader(value = "Authorization", required = true) String token,
            @PathVariable("username") String username);

}
