package com.example.freego.recipe.controller;




import com.example.freego.common.DTO.FoodDto;
import com.example.freego.common.DTO.RecipeDto;
import com.example.freego.recipe.Model.RecipeModel;
import com.example.freego.recipe.Service.RecipeService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
//TODO Icci on met des la recupératon des food ou pas ? si non il faut que se soit une liste d'id dans
@CrossOrigin
@RestController
public class RecipeRestController {

	@Autowired
	private RecipeService recipeService;

	@RequestMapping("/recipes")
	private List<RecipeDto> getAllRecipe() {
		List<RecipeDto> recipeDtos = new ArrayList<>();
		List<RecipeModel> r = recipeService.getAllRecipe();
		for(RecipeModel recipe: r){
			recipeDtos.add(recipe.toDto());
		}
		return recipeDtos;

	}

	@RequestMapping(method= RequestMethod.POST, value= "/recipes/foods")
	private List<RecipeModel> getAllRecipeByFoodsIds(@RequestBody List<Long> foods) {

		return recipeService.getAllRecipeByFoods(foods);
	}

	@RequestMapping("/recipes/{id}")
	private RecipeDto getRecipe(@PathVariable String id) {
		Optional<RecipeModel> rrecipe;
		rrecipe= recipeService.getRecipe(id);
		if(rrecipe.isPresent()) {
			return rrecipe.get().toDto();
		}
		return null;

	}

	@RequestMapping(method= RequestMethod.POST,value="/recipes")
	public void addRecipe(@RequestBody RecipeDto recipe) {
		recipeService.addRecipe(new RecipeModel(recipe));
	}

	@RequestMapping(method=RequestMethod.PUT,value="/recipes/{id}")
	public void updateRecipe(@RequestBody RecipeDto recipe) {
		recipeService.updateRecipe(new RecipeModel(recipe));
	}

	@RequestMapping(method=RequestMethod.DELETE,value="/recipes/{id}")
	public void deleteRecipe(@PathVariable String id) {
		recipeService.deleteRecipe(id);
	}

	@RequestMapping(method= RequestMethod.GET,value="/recipes/pages")
	private Page<RecipeDto> getAllFood(@RequestParam("number") Integer number, @RequestParam("size") Integer size,@RequestParam(value = "name", required=false) String name) throws NotFoundException {
		Pageable p = PageRequest.of(number,size);
		if(number > p.getPageNumber()){
			throw new NotFoundException("Le nombre de page est trop grand");
		}
		return recipeService.getAllFoodPaginated(p,name);


	}

}
