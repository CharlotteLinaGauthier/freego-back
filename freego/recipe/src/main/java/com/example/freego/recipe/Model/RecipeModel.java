package com.example.freego.recipe.Model;

import com.example.freego.common.DTO.RecipeDto;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class RecipeModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String imgurl;
    private String preparetime;
    private String cooktime;
    private String totaltime;
    @Column(length = 3000)
    private String directions;
    @ElementCollection
    @MapKeyColumn(name="ingredients")
    private Map<Long,String> idingredients = new HashMap<>();


    public RecipeModel() {
    }

    public RecipeModel(RecipeDto recipeDto){
        this.id = recipeDto.getId();
        this.name = recipeDto.getName();
        this.imgurl = recipeDto.getImgurl();
        this.preparetime = recipeDto.getPreparetime();
        this.cooktime = recipeDto.getCooktime();
        this.totaltime = recipeDto.getTotaltime();
        this.directions = recipeDto.getDirections();

    }

    public RecipeModel(Long id, String name, String imgurl, String preparetime, String cooktime, String totaltime, String directions, Map<Long, String> idingredients) {
        this.id = id;
        this.name = name;
        this.imgurl = imgurl;
        this.preparetime = preparetime;
        this.cooktime = cooktime;
        this.totaltime = totaltime;
        this.directions = directions;
        this.idingredients = idingredients;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getPreparetime() {
        return preparetime;
    }

    public void setPreparetime(String preparetime) {
        this.preparetime = preparetime;
    }

    public String getCooktime() {
        return cooktime;
    }

    public void setCooktime(String cooktime) {
        this.cooktime = cooktime;
    }

    public String getTotaltime() {
        return totaltime;
    }

    public void setTotaltime(String totaltime) {
        this.totaltime = totaltime;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    public Map<Long, String> getIdingredients() {
        return idingredients;
    }

    public void setIdingredients(Map<Long, String> idingredients) {
        this.idingredients = idingredients;
    }

    public RecipeDto toDto(){
        return new RecipeDto(this.id, this.name, this.imgurl, this.preparetime, this.cooktime, this.totaltime, this.directions, this.idingredients);
    }





}
