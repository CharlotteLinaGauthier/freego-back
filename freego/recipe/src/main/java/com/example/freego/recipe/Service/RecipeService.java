package com.example.freego.recipe.Service;



import com.example.freego.common.DTO.FoodDto;
import com.example.freego.common.DTO.RecipeDto;
import com.example.freego.recipe.Model.RecipeModel;
import com.example.freego.recipe.Repository.RecipeRepository;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;


@Service
public class RecipeService {

	@Autowired
	private RecipeRepository recipeRepository;



	public List<RecipeModel> getAllRecipe() {
		return new ArrayList<>(recipeRepository.findAll());
	}

	public List<RecipeModel> getAllRecipeByFoods(List<Long> foods) {
		List<RecipeModel> recipeList = new ArrayList<>();
		recipeList= recipeRepository.getAllByIngredients(foods);
		return recipeList;
	}


	public Page<RecipeDto> getAllFoodPaginated(Pageable pageable,String name) {
		List<RecipeDto> recipeList = new ArrayList<>();
		List<RecipeModel> recipeM = new ArrayList<>();
		if(name != null){
			recipeM = recipeRepository.findAllByNameContainingIgnoreCase(name);
		}
		else{
			recipeM = recipeRepository.findAll();
		}
		for(RecipeModel recipeModel : recipeM){
			recipeList.add(recipeModel.toDto());
		};
		int start = (int) pageable.getOffset();
		int end = (int) ((start + pageable.getPageSize()) > recipeList.size() ? recipeList.size()
				: (start + pageable.getPageSize()));
		Page<RecipeDto> page
				= new PageImpl<RecipeDto>(recipeList.subList(start, end), pageable, recipeList.size());
		return page;
	}

	public Optional<RecipeModel> getRecipe(String id) {
		return recipeRepository.findById(Long.valueOf(id));
	}

	public Optional<RecipeModel> getRecipe(Long id) {
		return recipeRepository.findById(id);
	}

	public void addRecipe(RecipeModel recipe) {

		// needed to avoid detached entity passed to persist error

		recipeRepository.save(recipe);
	}




	public void  updateRecipe(RecipeModel recipe) {
		recipeRepository.save(recipe);
	}

	public void deleteRecipe(String id) {
		recipeRepository.deleteById(Long.valueOf(id));
	}

	@EventListener(ApplicationReadyEvent.class)
	public void init() throws IOException, CsvException {
		boolean initBDD = false;
		if (initBDD) {
			List<RecipeModel> beans = new CsvToBeanBuilder(new FileReader(ResourceUtils.getFile("/src/main/resources/recipeFinale.csv")))
					.withType(RecipeModel.class)
					.withSeparator(';')
					.build()
					.parse();
			CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build();
			CSVReader reader = new CSVReaderBuilder(
					new FileReader(ResourceUtils.getFile("/src/main/resources/recipeIngredientsQuantity.csv")))
					.withCSVParser(csvParser)   // custom CSV parser
					.withSkipLines(1)           // skip the first line, header info
					.build();
			List<String[]> rFoods = reader.readAll();
			Map<Long, String> temp;
			for (RecipeModel bean : beans) {

				for (String[] str : rFoods) {
					if (Long.parseLong(str[0]) == bean.getId()) {
						temp = bean.getIdingredients();
						temp.put(Long.parseLong(str[1]), str[2]);
						bean.setIdingredients(temp);
					}
				}
				this.addRecipe(bean);
			}
		}
	}


}
