package com.example.freego.recipe.Repository;




import com.example.freego.recipe.Model.RecipeModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Map;

public interface RecipeRepository extends CrudRepository<RecipeModel, Long> {
	List<RecipeModel> findAll();
	List<RecipeModel> findAllByNameContainingIgnoreCase(String name);

	@Query(value= "select distinct on (rm) * from recipe_model rm left join recipe_model_idingredients rmi on rm.id = rmi.recipe_model_id where rmi.ingredients in (?1) order by rm,id",
			nativeQuery = true)
	List<RecipeModel> getAllByIngredients(List<Long> l);
}
