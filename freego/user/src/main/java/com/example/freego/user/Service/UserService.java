package com.example.freego.user.Service;


import com.example.freego.common.Client.FridgesClient;
import com.example.freego.common.DTO.FridgeDto;
import com.example.freego.common.DTO.UserDto;
import com.example.freego.common.DTO.UserDtoWithPassword;
import com.example.freego.common.Exception.FunctionalException;
import com.example.freego.user.Repository.PreferenceRepository;
import com.example.freego.user.Repository.UserRepository;
import com.example.freego.user.model.Preference;
import com.example.freego.user.model.UserModel;
import com.opencsv.*;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private FridgesClient fridgesClient;

	@Autowired
	private PreferenceRepository preferenceRepository;

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public List<Preference> getAllPreferences() {
		List<Preference> userList = new ArrayList<>();
		preferenceRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		return userRepository.findById(Long.valueOf(id));
	}

	public Optional<UserModel> getUser(Long id) {
		return userRepository.findById(id);
	}

	public UserModel getByUsername(String username){
		UserModel user = userRepository.findFirstByLogin(username);
		if(user == null){
			return new UserModel();
		}
		return user;}

	public UserDto addUser(UserModel user) {
		// needed to avoid detached entity passed to persist error
		System.out.println(user.getLogin());
		Preference p = user.getPreference();
		if(p == null || p.getId() == 0){
			p = preferenceRepository.findById(1L).get();
			user.setPreference(p);
		}
		UserModel u =  userRepository.save(user);
		fridgesClient.createFridge(new FridgeDto(0L,u.getLogin()+"'s fridge",u.getId(),new ArrayList<>()));
		return u.toDto();
	}

	public void updateUser(UserModel user)
	{
		Preference p = user.getPreference();
		if(p == null || p.getId() == 0){
			p = preferenceRepository.findById(1L).get();
			user.setPreference(p);
		}
		user.setPwd(userRepository.findById(user.getId()).get().getPwd());
		userRepository.save(user);
	}

	public void deleteUser(String id) {
		userRepository.deleteById(Long.valueOf(id));
	}

	public UserDto getFirstUserByLoginAndPwd(UserDtoWithPassword userDTO) throws FunctionalException {
		UserModel user = userRepository.findFirstByLoginAndPwd(userDTO.getLogin(),userDTO.getPwd());
		if(user==null){
			throw new FunctionalException("Ca ne marche pas.");
		}
		UserDto userDto  =user.toDto();
		return userDto;
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		List<UserModel> ulist=null;
		ulist=userRepository.findByLoginAndPwd(login,pwd);
		return ulist;
	}

	public void addAllergietoUser(Long userId,Long foodId){
		Optional<UserModel> u = userRepository.findById(userId);
		if(u.isPresent()){
			UserModel user = u.get();
			Set<Long> all = user.getAllergies();
			all.add(foodId);
			user.setAllergies(all);
			userRepository.save(user);
		}
	}

	@EventListener(ApplicationReadyEvent.class)
	public void init() throws IOException, CsvException {
		boolean initBDD = false;
		if (initBDD) {
			File directoryPath = new File("/src/main/resources");
			//List of all files and directories
			String contents[] = directoryPath.list();
			System.out.println("List of files and directories in the specified directory:");
			for (int i = 0; i < contents.length; i++) {
				System.out.println(contents[i]);
			}

			System.out.println("\n\n\n\n\n\n =========================================================\n");


			List<UserModel> beans = new CsvToBeanBuilder(new FileReader(ResourceUtils.getFile("/src/main/resources/user.csv")))
					.withType(UserModel.class)
					.withSeparator(';')
					.build()
					.parse();
			CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build();
			CSVReader readerAllergies = new CSVReaderBuilder(
					new FileReader(ResourceUtils.getFile("/src/main/resources/userAllergies.csv")))
					.withCSVParser(csvParser)   // custom CSV parser
					.withSkipLines(1)           // skip the first line, header info
					.build();
			List<String[]> rAllergies = readerAllergies.readAll();
			CSVReader readerFriends = new CSVReaderBuilder(
					new FileReader(ResourceUtils.getFile("/src/main/resources/userFriends.csv")))
					.withCSVParser(csvParser)   // custom CSV parser
					.withSkipLines(1)           // skip the first line, header info
					.build();
			List<String[]> rFriends = readerFriends.readAll();

			Set<Long> temp;
			for (UserModel bean : beans) {
				if (this.getByUsername(bean.getLogin()).getId() == null) {
					for (String[] str : rAllergies) {
						if (Long.parseLong(str[0]) == bean.getId()) {
							temp = bean.getAllergies();
							temp.add(Long.parseLong(str[1]));
							bean.setAllergies(temp);
						}
					}
					for (String[] str : rFriends) {
						if (Long.parseLong(str[0]) == bean.getId()) {
							temp = bean.getIdfriends();
							temp.add(Long.parseLong(str[1]));
							bean.setIdfriends(temp);
						}
					}
					this.updateUser(bean);
				}

			}

/*
		UserModel user=new UserModel("mat","gg");
		Set<Long> test = user.getAllergies();
		test.add(3L);
		test.add(4L);
		user.setAllergies(test);
		UserDto user2 = this.addUser(user);
		try(		Writer writer = Files.newBufferedWriter(ResourceUtils.getFile("classpath:test.csv").toPath());

				)
		{
			StatefulBeanToCsv<UserModel> beanToCsv = new StatefulBeanToCsvBuilder(writer)
					.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
					.withSeparator(';')
					.withMappingStrategy()
					.build();
			System.out.println("coucou "+ResourceUtils.getFile("classpath:test.csv").toPath());
			List<UserModel> myUsers = this.getAllUsers();
			System.out.println(myUsers.size());
			beanToCsv.write(myUsers);*/


		}
	}


}
