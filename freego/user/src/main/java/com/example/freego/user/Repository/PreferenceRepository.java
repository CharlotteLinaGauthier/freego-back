package com.example.freego.user.Repository;


import com.example.freego.user.model.Preference;
import com.example.freego.user.model.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PreferenceRepository extends CrudRepository<Preference, Long> {

}
