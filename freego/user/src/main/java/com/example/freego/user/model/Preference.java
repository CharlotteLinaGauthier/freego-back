package com.example.freego.user.model;

import com.example.freego.common.DTO.PreferenceDto;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.opencsv.bean.CsvBindByPosition;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Preference {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @CsvBindByPosition(position = 0)
    private Long id;
    private String name;

    public Preference() {
        this.id = 0L;
        this.name = "";
    }

    public Preference(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Preference(PreferenceDto p){
        this.id = p.getId();
        this.name = p.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PreferenceDto toDto(){
        return new PreferenceDto(this.id,this.name);
    }
}
