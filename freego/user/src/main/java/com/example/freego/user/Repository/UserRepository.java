package com.example.freego.user.Repository;


import com.example.freego.user.model.UserModel;
import org.apache.catalina.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<UserModel, Long> {
	List<UserModel> findByLoginAndPwd(String login, String pwd);
	UserModel findFirstByLoginAndPwd(String login,String pwd);
	UserModel findFirstByLogin(String login);
}
