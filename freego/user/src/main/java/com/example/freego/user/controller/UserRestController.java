package com.example.freego.user.controller;


import com.example.freego.common.DTO.PreferenceDto;
import com.example.freego.common.DTO.UserDto;
import com.example.freego.common.DTO.UserDtoWithPassword;
import com.example.freego.common.Exception.FunctionalException;
import com.example.freego.user.Service.UserService;
import com.example.freego.user.model.Preference;
import com.example.freego.user.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@Import(FeignClientsConfiguration.class)
@RestController
public class UserRestController {

	@Autowired
	private UserService userService;

	@RequestMapping("/users")
	private List<UserDto> getAllUsers() {
		List<UserDto> userDtos = new ArrayList<>();
		for(UserModel user:userService.getAllUsers()){
			userDtos.add(user.toDto());
		}
		return userDtos;

	}

	@RequestMapping("/users/id/{id}")
	private UserDto getUser(@PathVariable String id) {
		Optional<UserModel> ruser;
		ruser= userService.getUser(id);
		if(ruser.isPresent()) {
			return ruser.get().toDto();
		}
		return null;

	}

	@RequestMapping(method= RequestMethod.POST,value="/users")
	public UserDto addUser(@RequestBody UserDtoWithPassword user) {
		return userService.addUser(new UserModel(user));
	}

	@RequestMapping(method=RequestMethod.PUT,value="/users/{id}")
	public void updateUser(@RequestBody UserDtoWithPassword user) {
		userService.updateUser(new UserModel(user));
	}

	@RequestMapping(method=RequestMethod.DELETE,value="/users/{id}")
	public void deleteUser(@PathVariable String id) {
		userService.deleteUser(id);
	}

	@RequestMapping(method=RequestMethod.POST,value="/users/connect")
	public UserDto connectUser(@RequestBody UserDtoWithPassword userDTO) {
		System.out.println(userDTO);
		//throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

		try {
			return userService.getFirstUserByLoginAndPwd(userDTO);
		} catch (FunctionalException e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage(),e);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value="/users/{username}")
	public UserDtoWithPassword getByUsername(@PathVariable("username") String username){
		return userService.getByUsername(username).toDtoWithPassword();
	}

	@RequestMapping(method = RequestMethod.GET, value="/users/preferences/all")
	public List<PreferenceDto> getAllPreferences(){
		return userService.getAllPreferences().stream().map(Preference::toDto).collect(Collectors.toList());
	}

	@RequestMapping(method = RequestMethod.PUT, value="/users/allergie/{userId}/{foodId}")
	public void addAllergieToUser(@PathVariable Long userId, @PathVariable Long foodId){
		userService.addAllergietoUser(userId,foodId);
	}

}
