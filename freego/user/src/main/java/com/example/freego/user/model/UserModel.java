package com.example.freego.user.model;


import com.example.freego.common.DTO.UserDto;
import com.example.freego.common.DTO.UserDtoWithPassword;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.opencsv.bean.CsvBindByPosition;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserModel implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@CsvBindByPosition(position = 0)
	private Long id;
	@CsvBindByPosition(position = 1)
	private String login;
	@CsvBindByPosition(position = 2)
	private String pwd;
	private String lastName;
	private String surName;
	@CsvBindByPosition(position = 3)
	private String email;
	@CsvBindByPosition(position = 4)
	@ManyToOne
	private Preference preference;

	@ElementCollection(fetch = FetchType.EAGER )
	private Set<Long> allergies = new HashSet<>();

	@ElementCollection(fetch = FetchType.EAGER)
	private Set<Long> idfriends = new HashSet<>();

	//TODO Nutrition ? -> des stats a trouver sur le net
	//TODO Une liste de alergie, la liste comment la gérer ? string libre ? a cocher ?



	public UserModel() {
		this.login = "";
		this.pwd = "";
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public UserModel(UserDto userDto){
		this.id = userDto.getId();
		this.login = userDto.getLogin();
		this.pwd = "";
		if (userDto.getPreferenceId() == null) {
			this.preference = new Preference();
		}
		else{
			this.preference = new Preference(userDto.getPreferenceId(),userDto.getPreferenceName());
		}
		this.lastName=userDto.getLastName();
		this.surName=userDto.getSurName();
		this.email=userDto.getEmail();
		this.allergies = userDto.getAllergies();
	}

	public UserModel(UserDtoWithPassword userDto){
		this.id = userDto.getId();
		this.login = userDto.getLogin();
		this.pwd = userDto.getPwd();
		this.lastName=userDto.getLastName();
		this.surName=userDto.getSurName();
		this.email=userDto.getEmail();
		if (userDto.getPreferenceId() == null) {
			this.preference = new Preference();
		}
		else{
			this.preference = new Preference(userDto.getPreferenceId(),userDto.getPreferenceName());
		}
		this.allergies = userDto.getAllergies();
	}

	public UserModel(String login, String pwd) {
		super();
		this.login = login;
		this.pwd = pwd;
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}



	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Preference getPreference() {
		return preference;
	}

	public void setPreference(Preference preference) {
		this.preference = preference;
	}

	public Set<Long> getAllergies() {
		return allergies;
	}

	public void setAllergies(Set<Long> allergies) {
		this.allergies = allergies;
	}

	public Set<Long> getIdfriends() {
		return idfriends;
	}

	public void setIdfriends(Set<Long> idfriends) {
		this.idfriends = idfriends;
	}

	public UserDto toDto(){
		if(this.preference == null)
		{
			this.preference = new Preference();
		}
		return new UserDto(this.id, this.login, this.lastName, this.surName, this.email, this.preference.toDto(),this.allergies,this.idfriends);
	}

	public UserDtoWithPassword toDtoWithPassword(){
		if(this.preference == null)
		{
			this.preference = new Preference();
		}
		UserDto temp = new UserDto(this.id, this.login, this.lastName, this.surName, this.email, this.preference.toDto(),this.allergies,this.idfriends);
		return new UserDtoWithPassword(temp, this.pwd);
	}

}
