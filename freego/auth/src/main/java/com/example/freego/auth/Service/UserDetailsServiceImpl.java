package com.example.freego.auth.Service;

import com.example.freego.auth.Security.JwtUtils;
import com.example.freego.common.Client.UsersClient;
import com.example.freego.common.DTO.UserDto;
import com.example.freego.common.DTO.UserDtoWithPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UsersClient userClient;

    @Autowired
    JwtUtils jwtUtil;


    @Transactional
    public UserDetails loadUserByUsername(String jwt, String username) throws UsernameNotFoundException {
        UserDtoWithPassword user = userClient.getByUsername(jwt, username);
        if(user.getId() == null) {
            throw new UsernameNotFoundException("User Not Found with username: " + username);
        }
        return user;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDtoWithPassword user = userClient.getByUsername("",username);
        if(user.getId() == null) {
            throw new UsernameNotFoundException("User Not Found with username: " + username);
        }
        return user;
    }


    @Transactional
    public UserDetails createUser(UserDtoWithPassword userToCreate) throws UsernameNotFoundException {
        UserDto user = userClient.createUser(userToCreate);
        if(user.getId() == null) {
            throw new UsernameNotFoundException("User Not Found with username: " + user.getLogin());
        }
        return new UserDtoWithPassword(user,"");
    }


}