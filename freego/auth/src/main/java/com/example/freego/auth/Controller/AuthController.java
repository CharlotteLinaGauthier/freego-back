package com.example.freego.auth.Controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


import com.example.freego.auth.Model.JwtResponse;
import com.example.freego.auth.Security.JwtUtils;

import com.example.freego.auth.Service.UserDetailsServiceImpl;
import com.example.freego.common.Client.UsersClient;

import com.example.freego.common.DTO.SignupRequest;
import com.example.freego.common.DTO.UserDto;
import com.example.freego.common.DTO.UserDtoWithPassword;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired

    UserDetailsServiceImpl userDetailsService;

    @Autowired
    UsersClient userClient;


    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    @ResponseBody
    public ResponseEntity<?> authenticateUser(@RequestBody SignupRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDtoWithPassword userDetails = (UserDtoWithPassword) authentication.getPrincipal();

        /* GESTIO NDES ROLES

        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());*/

        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new JwtResponse(jwt,userDetails.toUserDto()).toString());

    }

    @PostMapping("/signup")
    @ResponseBody
    public ResponseEntity<?> registerUser(@RequestBody UserDtoWithPassword signUpRequest) {
        System.out.println("la route marche deja  c'est cool"+signUpRequest.getUsername());

        if (userClient.getByUsername("",signUpRequest.getUsername()).getId() != null) {
            return ResponseEntity
                    .badRequest()
                    .body("Error: Username is already taken!");
        }

        System.out.println("Apparement le user existe pas");

       /* CHECK SI LE MAIL EXISTE

       if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }*/

        // Create new user's account
        /*User user = new User(signUpRequest.getUsername(),
                signUpRequest.getEmail(),))*/
        String tempPass = signUpRequest.getPassword();
        signUpRequest.setPwd(encoder.encode(signUpRequest.getPassword()));
        System.out.println("j'ai codé le mdp :"+signUpRequest.getPassword());
        /*Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "mod":
                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);*/
        System.out.println("Si je vois ce message c'ets chiant");
        UserDto userDetails  = userClient.createUser(signUpRequest);
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(signUpRequest.getUsername(), tempPass));

        String jwt = jwtUtils.generateJwtToken(authentication);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new JwtResponse(jwt,userDetails).toString());
    }

    @RequestMapping("/me")
    @ResponseBody
    public UserDto getUserFromtoken(@RequestHeader (name="Authorization") String token){
        String username = jwtUtils.getUserNameFromJwtToken(token.substring(7));
        return userClient.getByUsername(token, username);
    }

}