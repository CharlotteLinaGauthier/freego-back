package com.example.freego.auth.Model;

import com.example.freego.common.DTO.UserDto;
import com.example.freego.common.DTO.UserDtoWithPassword;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private final String jwt;
    private final UserDto user;


    public JwtResponse(String jwt, UserDto user) {
        this.jwt= jwt;
        this.user = user;
    }

    @Override
    public String toString() {
        return "{" +
                "\"jwt\":\"" + jwt + "\"" +
                ", \"user\":" + user +
                '}';
    }
}

