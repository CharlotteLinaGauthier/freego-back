package com.example.freego.food.Model;

import com.example.freego.common.DTO.FoodDto;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.core.metrics.StartupStep;

import javax.persistence.*;

@Entity(name = "foodmodel")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class FoodModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String imgurl;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tags_id", nullable = false)
    private Tags tags;

    public FoodModel() {
    }

    public FoodModel(Long id, String name, String imgurl, Tags tags) {
        this.id = id;
        this.name = name;
        this.imgurl = imgurl;
        this.tags = tags;
    }

    public FoodModel(FoodDto foodDto) {

        this.id = foodDto.getId();
        this.name = foodDto.getName();
        this.imgurl = foodDto.getImgurl();

        this.tags = new Tags(foodDto.getTagsId(), foodDto.getTagsDescription());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FoodDto toFoodDto(){
        return new FoodDto(this.id,this.name,this.imgurl, this.tags.getId(),this.tags.getDescription());
    }





}
