package com.example.freego.food.Repository;



import com.example.freego.food.Model.FoodModel;
import net.bytebuddy.description.type.TypeDescription;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface FoodRepository extends CrudRepository<FoodModel, Long> {
	List<FoodModel> findAll();
	List<FoodModel> findAllByNameContaining(String name);
	List<FoodModel> findAllByIdIn(Set<Long> ids);
}
