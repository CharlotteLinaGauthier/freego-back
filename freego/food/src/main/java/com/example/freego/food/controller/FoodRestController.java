package com.example.freego.food.controller;



import com.example.freego.common.DTO.FoodDto;
import com.example.freego.common.DTO.UserDto;
import com.example.freego.food.Model.FoodModel;
import com.example.freego.food.Service.FoodService;
import javassist.NotFoundException;
import org.springframework.beans.NotReadablePropertyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
//TODO Ici on met des la recupératon des food ou pas ? si non il faut que se soit une liste d'id dans fridge
@CrossOrigin
@RestController
public class FoodRestController {

	@Autowired
	private FoodService foodService;

	@RequestMapping(method= RequestMethod.GET,value="/foods")
	private List<FoodDto> getAllFood() {
		List<FoodDto> foodDto = new ArrayList<>();
		for (FoodModel food:foodService.getAllFood()) {
			foodDto.add(food.toFoodDto());
		}
		return foodDto;

	}

	@RequestMapping(method= RequestMethod.GET,value="/foods/pages")
	private Page<FoodDto> getAllFood(@RequestParam("number") Integer number,@RequestParam("size") Integer size,@RequestParam(value = "name", required=false) String name) throws NotFoundException {
		Pageable p = PageRequest.of(number,size);
		if(number > p.getPageNumber()){
			throw new NotFoundException("Le nombre de page est trop grand");
		}
		return foodService.getAllFoodPaginated(p,name);


	}

	@RequestMapping(method= RequestMethod.POST,value="/foods/fridge")
	private List<FoodDto> getFoodsByFridgeId(@RequestBody List<Long>ids) {
		List<FoodDto> foodDto = new ArrayList<>();
		for (FoodModel food:foodService.getFoodsByFridgeId(ids)) {
			foodDto.add(food.toFoodDto());
		}
		return foodDto;

	}


	@RequestMapping(method= RequestMethod.GET,value="/foods/{id}")
	private FoodDto getFood(@PathVariable String id) {
		Optional<FoodModel> rfood;
		rfood= foodService.getFood(id);
		if(rfood.isPresent()) {
			return rfood.get().toFoodDto();
		}
		return null;

	}

	@RequestMapping(method= RequestMethod.POST,value="/foods/ids/user")
	private List<FoodDto> getAllFoodByIds(@RequestBody UserDto user) {
		List<FoodDto> foodDto = new ArrayList<>();
		for (FoodModel food:foodService.getAllFoodByIds(user.getAllergies())) {
			foodDto.add(food.toFoodDto());
		}
		return foodDto;

	}

	@RequestMapping(method= RequestMethod.POST,value="/foods")
	public void addFood(@RequestBody FoodDto food) {
		foodService.addFood(new FoodModel(food));
	}

	@RequestMapping(method=RequestMethod.PUT,value="/foods/{id}")
	public void updateFood(@RequestBody FoodDto food) {
		foodService.updateFood(new FoodModel(food));
	}

	@RequestMapping(method=RequestMethod.DELETE,value="/foods/{id}")
	public void deleteFood(@PathVariable String id) {
		foodService.deleteFood(id);
	}


}
