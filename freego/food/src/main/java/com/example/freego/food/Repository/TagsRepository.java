package com.example.freego.food.Repository;

import com.example.freego.food.Model.FoodModel;
import com.example.freego.food.Model.Tags;
import org.springframework.data.repository.CrudRepository;

public interface TagsRepository extends CrudRepository<Tags, Long> {
}
