package com.example.freego.food.Service;



import com.example.freego.common.DTO.FoodDto;
import com.example.freego.food.Model.FoodModel;
import com.example.freego.food.Model.Tags;
import com.example.freego.food.Repository.FoodRepository;
import com.example.freego.food.Repository.TagsRepository;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;



@Service
public class FoodService {

	@Autowired
	private FoodRepository foodRepository;

	@Autowired
	private TagsRepository tagsRepository;



	public List<FoodModel> getAllFood() {
		List<FoodModel> foodList = new ArrayList<>();
		foodRepository.findAll().forEach(foodList::add);
		return foodList;
	}

	public List<FoodModel> getAllFoodByIds(Set<Long> ids) {

		return foodRepository.findAllByIdIn(ids);
	}

	public Page<FoodDto> getAllFoodPaginated(Pageable pageable,String name) {
		List<FoodDto> foodList = new ArrayList<>();
		List<FoodModel> foodM = new ArrayList<>();
		if(name != null){
			foodM = foodRepository.findAllByNameContaining(name);
		}
		else {
			foodM = foodRepository.findAll();
		}

		for(FoodModel food : foodM){
			foodList.add(food.toFoodDto());
		};
		int start = (int) pageable.getOffset();
		int end = (int) ((start + pageable.getPageSize()) > foodList.size() ? foodList.size()
				: (start + pageable.getPageSize()));
		Page<FoodDto> page
				= new PageImpl<FoodDto>(foodList.subList(start, end), pageable, foodList.size());
		return page;
	}

	public Optional<FoodModel> getFood(String id) {
		return foodRepository.findById(Long.valueOf(id));
	}

	public Optional<FoodModel> getFood(Long id) {
		return foodRepository.findById(id);
	}

	public List<FoodModel> getFoodsByFridgeId(List<Long> ids){
		return getAllFood().stream().filter(foodModel -> ids.stream().anyMatch(o -> o.equals(foodModel.getId()))).collect(Collectors.toList());
	}

	public void addFood(FoodModel food) {
		// needed to avoid detached entity passed to persist error
		foodRepository.save(food);
	}

	public void updateFood(FoodModel food) {
		foodRepository.save(food);
	}

	public void deleteFood(String id) {
		foodRepository.deleteById(Long.valueOf(id));
	}


	@EventListener(ApplicationReadyEvent.class)
	public void init() throws IOException, CsvException, NoSuchFieldException {
		boolean initBDD = false;
		if (initBDD) {
		/*List<FoodModel> beans = new CsvToBeanBuilder(new FileReader(ResourceUtils.getFile("classpath:ingredients.csv")))
				.withType(FoodModel.class)
				.withSeparator(';')
				.withSkipLines(1)
				.build()
				.parse();*/
			CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build();
			CSVReader reader = new CSVReaderBuilder(
					new FileReader(ResourceUtils.getFile("/src/main/resources/ingredients.csv")))
					.withCSVParser(csvParser)   // custom CSV parser
					.withSkipLines(1)           // skip the first line, header info
					.build();
			CSVReader readerTags = new CSVReaderBuilder(
					new FileReader(ResourceUtils.getFile("/src/main/resources/categoryIngredients.csv")))
					.withCSVParser(csvParser)   // custom CSV parser
					.withSkipLines(1)           // skip the first line, header info
					.build();
			List<String[]> rFoods = reader.readAll();
			List<String[]> rTags = readerTags.readAll();
			Map<Long, String> temp;
			//for (FoodModel bean : beans) {

			Tags tags;
			for (String[] str : rTags) {
				tags = new Tags(Long.parseLong(str[0]), str[1]);
				tagsRepository.save(tags);
			}
			FoodModel food;
			for (String[] str : rFoods) {
				System.out.println(str[3]);
				food = new FoodModel(Long.parseLong(str[0]), str[1], str[2], tagsRepository.findById(Long.parseLong(str[3])).get());
				this.addFood(food);
			}
		}
	}



}
