package com.example.freego.fridge.repository;

import com.example.freego.fridge.model.FridgeFoodModel;
import com.example.freego.fridge.model.FridgeModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FridgeFoodRepository extends CrudRepository<FridgeFoodModel, Long> {
    List<FridgeFoodModel> getFridgeFoodModelByFoodId(Long foodId);
}
