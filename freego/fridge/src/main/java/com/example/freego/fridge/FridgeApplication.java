package com.example.freego.fridge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@EnableFeignClients(basePackages = {"com.example.freego.common.Client"})
@SpringBootApplication
public class FridgeApplication {
    public static void main(String[] args) {
        SpringApplication.run(FridgeApplication.class, args);
    }
}
