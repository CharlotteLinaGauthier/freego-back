package com.example.freego.fridge.service;


import com.example.freego.common.Client.FoodsClient;
import com.example.freego.common.DTO.FoodDto;
import com.example.freego.common.DTO.FridgeFoodDto;
import com.example.freego.fridge.model.FridgeFoodModel;
import com.example.freego.fridge.model.FridgeModel;
import com.example.freego.fridge.repository.FridgeFoodRepository;
import com.example.freego.fridge.repository.FridgeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.plaf.ListUI;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class FridgeService {

    @Autowired
    FridgeRepository fridgeRepository;

    @Autowired
    FridgeFoodRepository fridgeFoodRepository;

    @Autowired
    FoodsClient foodsClient;

    public List<FridgeModel> getAllFridges() {
        return (List<FridgeModel>) fridgeRepository.findAll();
    }

    public Optional<FridgeModel> getFridgeById(Long fridgeId){
        return fridgeRepository.getFridgeModelById(fridgeId);
    }

    public List<FridgeModel>  getFridgeByUserId(Long userId){
        return fridgeRepository.getFridgeModelByUserId(userId);
    }

    public void addFridge(FridgeModel fridgeModel){
        fridgeRepository.save(fridgeModel);
    }

    public void updateFridge(FridgeModel fridge){
        if(fridge.getId() != null){
            fridgeRepository.save(fridge);
        }
    }

    public void updateFridgeFoodList(String id, List<FridgeFoodDto> foodList){
        FridgeModel fridge = fridgeRepository.findById(Long.valueOf(id)).get();
        fridge.setFoodList(new HashSet<>(foodList.stream().map(fridgeFoodDto -> new FridgeFoodModel(fridgeFoodDto,fridge)).collect(Collectors.toList())));
        this.updateFridge(fridge);
    }




    public void addFoodToFridge(String id, FridgeFoodDto food){
        FridgeModel fridge = fridgeRepository.findById(Long.valueOf(id)).get();
        List<FridgeFoodModel> ffm = fridgeFoodRepository.getFridgeFoodModelByFoodId(food.getFoodId());
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        for(FridgeFoodModel f : ffm){
            if(fmt.format(food.getDlc()).compareTo(fmt.format(f.getDlc())) == 0 && f.getFridge().getId() == fridge.getId()){
                f.setQuantity(f.getQuantity()+food.getQuantity());
                this.updateFridge(fridge);
                return;
            }
        }
        Set<FridgeFoodModel> temp = fridge.getFoodList();
        temp.add(new FridgeFoodModel(food,fridge));
        fridge.setFoodList(temp);
        this.updateFridge(fridge);
    }

    public void deleteFridge(Long id){
        Optional<FridgeModel> f = getFridgeById(id);
        f.ifPresent(fridgeModel -> fridgeRepository.delete(fridgeModel));

    }

    public void deleteFridgeFoodModel(Long id){
        Optional<FridgeFoodModel> f = fridgeFoodRepository.findById(id);
        f.ifPresent(
                fridgeModel -> {
                    FridgeModel fm = fridgeModel.getFridge();
                    Set<FridgeFoodModel> sf = fm.getFoodList();
                    sf.remove(fridgeModel);
                    fm.setFoodList(sf);
                    fridgeRepository.save(fm);
                    fridgeFoodRepository.delete(fridgeModel);
                }
        );

    }

    public void updateFridgeFoodModel(FridgeFoodDto updateFridgeFoodModel){
                    Optional<FridgeFoodModel> temp = fridgeFoodRepository.findById(updateFridgeFoodModel.getId());
                    temp.ifPresent(
                      fridgeFood ->
                      {
                          FridgeFoodModel f = new FridgeFoodModel(updateFridgeFoodModel,fridgeFood.getFridge());
                          fridgeFoodRepository.save(f);
                      }
                    );

    }


    public List<FoodDto> getFoodsFromFridgeId(List<Long> ids){
        return foodsClient.getFoodsByFridgeId(ids);
    }
}
