package com.example.freego.fridge.model;


import com.example.freego.common.DTO.FridgeDto;
import com.example.freego.common.DTO.FridgeFoodDto;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Entity(name = "fridgemodel")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class FridgeModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private Long userId;
    @OneToMany( fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, mappedBy = "fridge")
    private Set<FridgeFoodModel> foodList = new HashSet<>();

    public FridgeModel(FridgeDto fridgeDto){
        this.id = fridgeDto.getId();
        this.name = fridgeDto.getName();
        this.userId = fridgeDto.getUserId();
        List<FridgeFoodModel> f = fridgeDto.getFoodList().stream().map(fridgeFoodDto -> new FridgeFoodModel(fridgeFoodDto,this)).collect(Collectors.toList());
        this.foodList = new HashSet<FridgeFoodModel>(f);
    }

    public FridgeModel() {

    }

    public FridgeModel(Long id, String name, Long userId, List<FridgeFoodModel> foodList) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.foodList = new HashSet<>(foodList);
    }


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<FridgeFoodModel> getFoodList() {
        return foodList;
    }

    public void setFoodList(Set<FridgeFoodModel> foodList) {
        this.foodList = foodList;
    }

    public FridgeDto toDto(){
        return new FridgeDto(this.id,this.name,this.userId);
    }

    public FridgeDto toDtoWithFood(List<FridgeFoodDto> foods){
        return new FridgeDto(this.id,this.name,this.userId,foods);
    }
}
