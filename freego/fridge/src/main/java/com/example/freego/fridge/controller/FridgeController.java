package com.example.freego.fridge.controller;


import com.example.freego.common.Client.FoodsClient;
import com.example.freego.common.DTO.FoodDto;
import com.example.freego.common.DTO.FridgeDto;
import com.example.freego.common.DTO.FridgeFoodDto;
import com.example.freego.fridge.model.FridgeModel;
import com.example.freego.fridge.service.FridgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
//TODO Metre le lien avec les food et une option avec food et sans food (voir si faut modifier les fridges)
@CrossOrigin
@RestController
public class FridgeController {

    @Autowired
    private FridgeService fridgeService;

    @Autowired
    private FoodsClient foodsClient;

    @RequestMapping("/fridges")
    private List<FridgeDto> getAllFridges() {
        List<FridgeDto> fridgeDtos = new ArrayList<>();
        for(FridgeModel fridge: fridgeService.getAllFridges()){
            fridgeDtos.add(fridge.toDto());
        }
        return fridgeDtos;

    }

    @RequestMapping("/fridges/{id}")
    private FridgeDto getFridge(@RequestParam(value = "foods", required = false) Boolean food,@PathVariable String id) {
        Optional<FridgeModel> f;
        if(food == null){
            food = false;
        }
        f= fridgeService.getFridgeById(Long.valueOf(id));
        assert f.orElse(null) != null;

        if(food){
            List<FridgeFoodDto> foods = f.get().getFoodList().stream().map(fridgeFoodModel -> fridgeFoodModel.toDto()).collect(Collectors.toList());
            List<Long> ids = new ArrayList<Long>();
            foods.forEach(fridgeFoodDto ->  ids.add(fridgeFoodDto.getFoodId()));
            List<FoodDto> foodsDto = foodsClient.getFoodsByFridgeId(ids);
            if(!ids.isEmpty()){
                foods= foods.stream().map(fridgeFoodDto -> new FridgeFoodDto(foodsDto.stream().filter(foodDto -> foodDto.getId() == fridgeFoodDto.getFoodId()).findFirst().get(), fridgeFoodDto.getId(), fridgeFoodDto.getDlc(),fridgeFoodDto.getQuantity(),fridgeFoodDto.getFoodId())).collect(Collectors.toList());
            }            return f.orElse(null).toDtoWithFood(foods);
        }
        return f.orElse(null).toDto();
    }

    @RequestMapping("/fridges/user/{id}")
    private List<FridgeDto> getFridgeByUserId(@RequestParam(value="foods", required = false) Boolean food, @PathVariable String id) {
        List<FridgeModel> f;
        List<FridgeDto> fridgeDtos = new ArrayList<>();
        if(food == null){
            food = false;
        }
        for(FridgeModel fridge: fridgeService.getFridgeByUserId(Long.valueOf(id))){
            if(food){
                List<FridgeFoodDto> foods = fridge.getFoodList().stream().map(fridgeFoodModel -> fridgeFoodModel.toDto()).collect(Collectors.toList());
                List<Long> ids = new ArrayList<Long>();
                foods.forEach(fridgeFoodDto ->  ids.add(fridgeFoodDto.getFoodId()));
                List<FoodDto> foodsDto = foodsClient.getFoodsByFridgeId(ids);
                if(!ids.isEmpty()){
                    foods= foods.stream().map(fridgeFoodDto -> new FridgeFoodDto(foodsDto.stream().filter(foodDto -> Objects.equals(foodDto.getId(), fridgeFoodDto.getFoodId())).findFirst().get(), fridgeFoodDto.getId(), fridgeFoodDto.getDlc(),fridgeFoodDto.getQuantity(),fridgeFoodDto.getFoodId())).collect(Collectors.toList());
                }
                foods = foods.stream().sorted(Comparator.comparing(FridgeFoodDto::getName)).sorted(Comparator.comparing(FridgeFoodDto::getDlc)).
                        collect(Collectors.toList());
                fridgeDtos.add(fridge.toDtoWithFood(foods));
            }
            else{
                fridgeDtos.add(fridge.toDto());
            }
        }

        return fridgeDtos;

    }

    @RequestMapping(method= RequestMethod.POST,value="/fridges")
    public void addFridge(@RequestBody FridgeDto fridge) {
        fridgeService.addFridge(new FridgeModel(fridge));
    }

    @RequestMapping(method= RequestMethod.PUT,value="/fridges/{id}")
    public void updateFoodList(@RequestBody List<FridgeFoodDto> foodList, @PathVariable String id) {
        fridgeService.updateFridgeFoodList(id,foodList);
    }

    @RequestMapping(method= RequestMethod.PUT,value="/fridges/addFood/{id}")
    public void addFoodToList(@RequestBody FridgeFoodDto foodDto, @PathVariable String id) {
        if(foodDto.getFoodId() == null){
            return;
        }
        fridgeService.addFoodToFridge(id,foodDto);
    }



    @RequestMapping(method=RequestMethod.PUT,value="/fridges")
    public void updateFridge(@RequestBody FridgeDto fridge) {
        fridgeService.updateFridge(new FridgeModel(fridge));
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/fridges/{id}")
    public void deleteFridge(@PathVariable String id) {
        fridgeService.deleteFridge(Long.valueOf(id));
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/fridges/food/{id}")
    public void deleteFridgeFoodModel(@PathVariable String id) {
        fridgeService.deleteFridgeFoodModel(Long.valueOf(id));
    }

    @RequestMapping(method= RequestMethod.PUT, value = "/fridges/food")
    public void updateFridgeFoodModel(@RequestBody FridgeFoodDto fridgeFoodDto){
        fridgeService.updateFridgeFoodModel(fridgeFoodDto);
    }


}
