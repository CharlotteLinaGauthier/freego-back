package com.example.freego.fridge.model;


import com.example.freego.common.DTO.FridgeFoodDto;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "fridgefoodmodel")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class FridgeFoodModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date dlc;
    private Integer quantity;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "fridge_id", nullable = false)
    private FridgeModel fridge;
    private Long foodId;

    public FridgeFoodModel() {
    }

    public FridgeFoodModel(Long id, Date dlc, Integer quantity, FridgeModel fridge, Long foodId) {
        this.id = id;
        this.dlc = dlc;
        this.quantity = quantity;
        this.foodId = foodId;
        this.fridge = fridge;
    }

    public FridgeFoodModel( Date dlc, Integer quantity, FridgeModel fridge, Long foodId) {
        this.dlc = dlc;
        this.quantity = quantity;
        this.foodId = foodId;
        this.fridge = fridge;
    }

    public FridgeFoodModel(FridgeFoodDto fridgeFoodDto, FridgeModel f){
        this.id = fridgeFoodDto.getId();
        this.dlc = fridgeFoodDto.getDlc();
        this.quantity = fridgeFoodDto.getQuantity();
        this.foodId = fridgeFoodDto.getFoodId();
        this.fridge = f;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDlc() {
        return dlc;
    }

    public void setDlc(Date dlc) {
        this.dlc = dlc;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public FridgeModel getFridge() {
        return fridge;
    }

    public void setFridge(FridgeModel fridge) {
        this.fridge = fridge;
    }

    public Long getFoodId() {
        return foodId;
    }

    public void setFoodId(Long foodId) {
        this.foodId = foodId;
    }

    public FridgeFoodDto toDto(){
        return new FridgeFoodDto(this.id,this.dlc,this.quantity,foodId);
    }
}
