package com.example.freego.fridge.repository;

import com.example.freego.fridge.model.FridgeModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FridgeRepository extends CrudRepository<FridgeModel, Long> {
    List<FridgeModel> getFridgeModelByUserId(Long userId);
    Optional<FridgeModel> getFridgeModelById(Long id);
}
